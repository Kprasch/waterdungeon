import random

"""
This is a-maze-ing Maze. You start it by calling the main function.
Exiting the program is always an option with 'exit'.
"""

def main():
    print("Welcome to the A-Maze-ing Maze! I dare you to escape!")
    quit_options = ['exit', 'quit', 'e', 'q']
    current_room = room0()

    while current_room not in quit_options:
        if randomizer():
            #box()
            current_room()
        else:
            current_room = current_room()
            current_room()




def process_user_movement(description, doors, lever=False):
    """
    This is the process_user_movement function that will
       handle a user's input.

    Args:
        doors: A description of the current room
        description: dictionary with door:location sets
    """
    # create list of quit options
    quit_options = ['exit', 'quit', 'e', 'q']


    # Print the description of the current room
    print(description)

    if lever == True:
        lever_action = input('Do you want to pull, push, or do nothing to the lever?')
        if lever_action.casefold() == 'nothing':
            print("Nothing happens.")
        elif lever_action.casefold() == 'push':
            print("Another door appears in the south wall.")
            doors = {"south": room6, "west": room4}
        elif lever_action.casefold() == 'pull':
            print("A trap door opens beneath your feet and you find yourself back at the start.")
            room0()
        else:
            print("Sorry, we don't understand. Please try again.")

    # Print the available doors
    print("You see doors to the ", end="")
    print(*list(doors.keys()), sep=", ")

    # Prompt the user for what doors they want
    choice = ""

    # Do things based on their response
    while choice.lower() not in quit_options:
        choice = input("Which way you would like to go? >> ")

        # Valid response: Go to the correct location
        if choice.lower() in doors:
            return doors[choice.lower()]
        elif choice.lower() in quit_options:
            print("Too tough for you, eh? Good-bye, then.")
            return exit
        # Invalid response: Ask them again
        else:
            print("I'm sorry, I didn't understand.")


def box():
    """
    user finds a box that could be anything!
    """
    user_number = int(input("You've found a box in this room! Enter a number between 1 and 10: "))
    number_one = random.randint(1, 10)
    number_two = random.randint(1, 10)
    number_three = random.randint(1, 10)
    prize_list = [number_one, number_two, number_three]
    if user_number in prize_list:
        print("You've found {} treasure coins!").format(user_number*100)
    elif user_number == 7:
        print("Oh no! You've been teleported back to the beginning!!!")
        main()
    else:
        print("The box was empty!")

def randomizer():
    num_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
    rand = random.randint(0, 10)
    if rand in num_list:
        return True
    else:
        return False


def room0():
    # description
    description = "This room is very small. You barely fit here. You're like Alice in that crazy-tiny-room thing."
    # doors
    # where those doors Go
    doors = {"east": room2, "west": room1}

    return process_user_movement(description, doors)


def room1():
    # description
    description = "This room is HUGE, but there is a tiny door to the south."
    # doors
    # where those doors Go
    doors = {"south": room3, "east": room0}

    return process_user_movement(description, doors)


def room2():
    # description
    description = "This room is boring, there really isn't anything here"
    # doors
    # where those doors Go
    doors = {"west": room0}

    return process_user_movement(description, doors)


def room3():
    # description
    description = "There are three doors here. They all look so inviting."
    # doors
    # where those doors Go
    doors = {"north": room1, "east": room6, "south": room4}

    return process_user_movement(description, doors)


def room4():
    # description
    description = "There are two doors here. They all look so inviting."
    # doors
    # where those doors Go
    doors = {"north": room3, "east": room5}

    return process_user_movement(description, doors)


def room5():
    # description
    description = "There is a giant lever in the middle of the room."
    # doors
    # where those doors Go
    doors = {"west": room4}

    return process_user_movement(description, doors, lever=True)


def room6():
    description = "Hey you found the exit!!"
    # doors
    # where those doors Go
    doors = {"exit": exit}

    return process_user_movement(description, doors)

if __name__ == '__main__':
    main()
